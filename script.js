//#4-5
let trainer = {};
trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends = { hoenn: ["May", "Max"], kanto: ["Brock", "Misty"]};
trainer.talk = function() {
    console.log("Pikachu! I choose you!");
};

console.log(trainer)

//dot notation
console.log("Result of dot notation: ");
console.log(trainer.name);
//console.log(trainer.friends.kanto);



//bracket notation
console.log("Result of bracket notation: ");
//console.log(trainer["name"]);
console.log(trainer["pokemon"]);

console.log("Result of talk method: ");
trainer.talk();

//#8-12
function Pokemon(name, level) {
    this.name = name;
    this.level = level*2;
    this.health = level;
    this.attack = level; 
    // Methods           //targetPokemon is object data type
    this.tackle = function(target){
                  //pokemonObject       //targetPokemon
        console.log(this.name +" tackled "+ target.name);
        
        console.log(`${target.name} health is now reduced to ${target.health - this.health}`);
        
        target.faint();

    }
    this.faint = function(){
        console.log(this.name+ " fainted.");
}
    }

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);

let pikachu2 = new Pokemon("Pikachu", 16);
console.log(pikachu2);
mewtwo.tackle(geodude);

